module.exports = {
  verbose: true,
  "collectCoverageFrom" : [
    "src/**/*.{js,jsx}",
    "!<rootDir>/node_modules/",
    "!<rootDir>/path/to/dir/"
  ],
  testPathIgnorePatterns: [
    "/node_modules/",
    "/lib/"
  ],
  setupFiles: [
    "<rootDir>/jest.setup.js"
  ]
}
