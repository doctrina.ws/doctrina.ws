const resolvers = {
  "@config": "./src/config",
  "@services": "./src/services",
  "@constants": "./src/constants"
}

module.exports = {
  presets: [
    [
      "@babel/env",
      {
        corejs: "3",
        useBuiltIns: "entry",
        targets: {
          node: "current"
        }
      }
    ]
  ],
  plugins: [
    ["module-resolver", { "alias": resolvers }]
  ]
}
