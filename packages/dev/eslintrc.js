module.exports = {
  parser: "babel-eslint",
  extends: [
    "eslint:recommended",
    "standard"
  ],
  env: {
    browser: true,
    es6: true
  },
  rules: {
    "semi": ["error", "never"],
    "quotes": ["error", "double"],
    "padded-blocks": 0,
    "curly": 0
  }
}
