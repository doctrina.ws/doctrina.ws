const Server = require("@doctrina.ws/server")
const Router = require("@doctrina.ws/router/Router")

// Define routes.
const routes = {
  "post.full": {
    pattern: "/post/:id",
    handle: ({ request, response, context }) => {
      const { route, router } = context
      let html = ""
      html += `<div>Post: ${route.params.id}</div>`
      html += `<a href="/page/10">Page: 10</a>`
      response.setHeader("Content-Type", "text/html")
      response.statusCode = 200
      response.end(html)
    }
  },
  "page.full": {
    pattern: "/page/:id",
    handle: ({ request, response, context }) => {
      const { route, router } = context
      let html = ""
      html += `<div>Page: ${route.params.id}</div>`
      html += `<a href="/post/10">Post: 10</a>`
      response.setHeader("Content-Type", "text/html")
      response.statusCode = 200
      response.end(html)
    }
  }
}

// Initialise router.
const router = new Router({ routes })

// Initialise server with router middleware.
const server = new Server()
server.use(({ request, response }) => {
  const context = { router }
  router.resolve({ request, response, context })
})

// Boot server.
const PORT = 8888
server.listen(PORT, error => {
  if (error) return console.log(error)
  console.log("Server Listening: " + PORT)
})
