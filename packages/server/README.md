# @doctrina.ws/server

A node HTTP server.

## Usage

```js
import Server from "@doctrina.ws/server"

// Prepare application server with middleware.
const server = new Server()
server.use(({ request, response }) => {
  response.send("Hello.")
  response.statusCode = 200
})

// Boot server .
const PORT = 8888
server.listen(PORT, error => {
  if (error) return console.log(error)
  console.log("Server Listening: " + PORT)
})
```
