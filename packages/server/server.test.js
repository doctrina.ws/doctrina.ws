const http = require("http")
const Server = require("./")

const server = new Server()

const request = ({ url, callback }) => new Promise(resolve => {
  http.get(url, response => resolve( callback(response) ))
})

beforeAll(() => {
  server.listen(1337, () => "Created.")
})

afterAll(() => {
  server.instance.close()
})

afterEach(() => {
  server.context = {}
  server.middleware = []
})

it("Sets response header", async () => {
  server.use(({ request, response }) => {
    response.setHeader("foo", "foo")
    response.statusCode = 200
    response.end()
  })

  const response = await request({
    url: "http://localhost:1337",
    callback: response => response
  })

  const {
    statusCode,
    headers: { foo }
  } = response

  expect({ foo, statusCode })
    .toEqual({ foo: "foo", statusCode: 200 })
})

it("Uses multiple middleware", async () => {
  server.use(({ request, response }) => {
    response.setHeader("bar", "bar")
  })

  server.use(({ request, response }) => {
    response.setHeader("bing", "bing")
  })

  server.use(({ request, response }) => {
    response.statusCode = 200
    response.end()
  })

  const response = await request({
    url: "http://localhost:1337",
    callback: response => response
  })

  const {
    statusCode,
    headers: { bar, bing }
  } = response

  expect({ bar, bing, statusCode })
    .toEqual({ bar: "bar", bing: "bing", statusCode: 200 })
})

it("Sets response header from context", async () => {
  server.context.bong = "bong"

  server.use(({ request, response, bong }) => {
    response.setHeader("bong", bong)
    response.statusCode = 200
    response.end()
  })

  const response = await request({
    url: "http://localhost:1337",
    callback: response => response
  })

  const {
    statusCode,
    headers: { bong }
  } = response

  expect({ bong, statusCode })
    .toEqual({ bong: "bong", statusCode: 200 })
})
