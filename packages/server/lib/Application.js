const http = require("http")

/**
 * Server.
 */
class Application {

  /**
   * Constructor.
   */
  constructor () {
    this.middleware = []
    this.context = {}
    this.instance = null
  }

  /*
   * Create HTTP listener.
   *
   * Listens and responds to incoming requests. Invoked on every request.
   */
  createListener () {
    return (request, response) => {
      const context = { request, response, ...this.context }
      this.middleware.map(fn => this.handleRequest({ context, fn }))
    }
  }

  /**
   * Request error handler.
   *
   * @param error {Error} - Request error.
   */
  handleError (error) {
    const message = error.stack || error.toString()
    console.error(message)
  }

  /**
   * Handle middleware request.
   *
   * @param context {object} - Request context.
   * @param fn {function} - Middleware handler.
   * @returns {Promise} - Request promise.
   */
  handleRequest ({ context, fn }) {
    const { response } = context
    // Default status code to 404.
    response.statusCode = 404
    return fn(context).catch(this.handleError)
  }

  /**
   * Attach middleware.
   *
   * @param fn {function} - Middleware handler.
   */
  use (fn) {
    this.middleware.push(context => {
      try { return Promise.resolve(fn(context)) }
      catch (error) { return Promise.reject(error) }
    })
  }

  /**
   * Create HTTP server.
   *
   * Boots HTTP server and attaches instance.
   */
  listen (...args) {
    const listener = this.createListener()
    const server = http.createServer(listener)
    this.instance = server
    server.listen(...args)
  }

}

module.exports = Application
