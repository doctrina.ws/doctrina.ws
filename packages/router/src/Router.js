const route = require("./route")

/**
 * Router.
 */
class Router {

  /**
   * Constructor.
   *
   * @param routes {object} - Routes definition.
   */
  constructor ({ routes }) {
    this.routes = routes || {}
    this.addRoutes({ routes })
  }

  /**
   * Add route.
   *
   * @param id {string} - Route ID
   * @param route {object} - Route definition.
   */
  add ({ id, route }) {
    this.routes[route.pattern] = { id, ...route }
  }

  /**
   * Add list of routes.
   *
   * @param routes {object} - Route definitions.
   */
  addRoutes ({ routes }) {
    Object.entries(routes).map(([id, route]) => {
      this.add({ id, route })
    })
  }

  /**
   * Match/execute request with route.
   *
   * @param request {object} - HTTP request.
   * @param response {object} - HTTP response.
   * @param context {object} - Route context.
   */
  resolve ({ request, response = {}, context = {} }) {
    const { url } = request

    // Holds resolved route values.
    let values

    // Find a matching route, stopping on first found.
    const match = Object.entries(this.routes)
      .find(([key, definition]) => {
        const resolver = route.getRoute(definition)
        if (resolver) {
          values = resolver.match({ url })
          if (values) return true
        }
      })

    // Trigger route handler.
    if (match) return new Promise((resolve, reject) => {
      const [id, definition] = match
      context.route = { id, ...values }
      resolve(definition.handle({ request, response, context }))
    })
  }

}

module.exports = Router
