const { getRoute } = require("./route")

it ("Matches a route.", () => {
  const route = getRoute({ pattern: "/foo/:bar/:baz" })
  const match = route.match({ url: "/foo/bar/baz" })
  expect(match).toStrictEqual({
    params: {
      bar: "bar",
      baz: "baz"
    }
  })
})
