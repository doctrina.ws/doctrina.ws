const Router = require("./Router")

it("Resolves a route", async () => {
  const routes = {
    foo: {
      pattern: "/hello/:name",
      handle: ({ request, response, context }) =>
        `Hello, ` + context.route.params.name
    }
  }

  const router = new Router({ routes })
  const request = { url: "/hello/John"}
  const response = await router.resolve({ request })
  expect(response).toEqual("Hello, John")
})

it("Resolves a route with context", async () => {
  const routes = {
    foo: {
      pattern: "/hello/:name",
      handle: ({ request, response, context }) =>
        `Hello, ` + context.name
    }
  }

  const router = new Router({ routes })
  const request = ({ url: "/hello/John" })
  const context= { name: "John" }
  const response = await router.resolve({ request, context })
  expect(response).toEqual("Hello, John")
})
