# @doctrina.ws/router

A universal HTTP router.

## Usage

``` js
import Router from "@doctrina.ws/router/Router"

// Define routes.
const routes = {
  "foo": {
    pattern: "/foo",
    handle: () => console.log("Handled.")
  }
}

const router = new Router({ routes })
router.resolve({ url: "/foo" })
// => Handled.
````

``` js
import { getRoute } from "@doctrina.ws/router/route"

const route = getRoute({ pattern: "/foo/:bar" })
route.match({ url: "/foo/bar" })
// => { params: { bar: "bar" } }
````
