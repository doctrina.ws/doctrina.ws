"use strict";

/**
 * Escape string for RegExp expression.
 *
 * @url https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#Escaping
 * @param {string} str - Raw Regex string.
 * @returns {string} - Encoded RegExp expression.
 */
const escapeString = str => str.replace(/[.*+?^${}()|[\]/\\]/g, "\\$&");
/**
 * Build route tokens.
 *
 * @param {string} pattern - Route pattern.
 * @returns {array} - Route tokens.
 */


const getTokens = ({
  pattern
}) => {
  const tokens = [];
  let result;
  let literal; // Pointers to "current" Used to capture and build literal sections in
  // routes, eg: `/foo` and `/baz` in `/foo/:bar/baz`.

  let ptrCurrent = 0; // Matches named parameters in a string.
  // eg: `/:bar` in `/foo/:bar/baz`.

  const matchParams = new RegExp("(:(\\w+))", "g"); // Loop route parameters.

  while ((result = matchParams.exec(pattern)) !== null) {
    // Push previous literal section.
    literal = pattern.slice(ptrCurrent, result.index);
    tokens.push(escapeString(literal));
    ptrCurrent = result.index; // Pattern captures a group in between literals.

    const group = "[^\\/]+?";
    const name = result[2];
    ptrCurrent += name.length + 1;
    tokens.push({
      name,
      pattern: group
    });
  } // If no named parameters were found, or remaining literals have not been
  // pushed, push the substring as a literal token.


  literal = pattern.slice(ptrCurrent);
  if (literal) tokens.push(escapeString(literal));
  return tokens;
};
/**
 * Extract route data from URL.
 *
 * @param exp {RegExp} - Route regex.
 * @param tokens {array} - Route tokens.
 * @param url {string} - URL.
 * @returns {object} Route data.
 */


const getData = ({
  exp,
  tokens,
  url
}) => {
  let idx = 0;
  const data = {
    params: {}
  };
  const result = exp.exec(url); // Exit early if route was not matched.

  if (!result) return; // Remove full string from result.

  result.shift(); // Populate params.

  tokens.map(token => {
    if (typeof token !== "object") return;
    const {
      name
    } = token;
    data.params[name] = result[idx];
    idx++;
  });
  return data;
};
/**
 * Build route object.
 *
 * @param {string} pattern - Route pattern.
 * @param {array} keys - Route keys.
 * @returns {object} Route.
 */


const getRoute = ({
  pattern
}) => {
  let route = "^";
  const tokens = getTokens({
    pattern
  });
  tokens.map(token => {
    if (typeof token === "string") route += token;else route += `(${token.pattern})`;
  }); // Conditionally match trailing `/`.

  route += "\\/?$";
  const exp = new RegExp(route);

  const match = ({
    url
  }) => getData({
    exp,
    tokens,
    url
  });

  return {
    exp,
    tokens,
    match
  };
};

module.exports = {
  getRoute
};