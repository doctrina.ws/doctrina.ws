---
title: Doctrina.
lede: Modern Web Development Research.
---
The web ecosystem is changing rapidly. Systems are becoming complex
while more and more frameworks are popularised. Doctrina is a research
project which aims to illustrate the ideas behind modern web development
patterns and how they work.

[View Archives](/archives)
